import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import UserLogin from "../views/UserLogin.vue";
import UserRegistration from "../views/UserRegistration.vue";
import UserConfirmSignup from "../views/UserConfirmSignup.vue";
import UserForgotPassword from "../views/UserForgotPassword.vue";
import UserPasswordRecovery from "../views/UserPasswordRecovery.vue"
import Dashboard from "../views/Dashboard.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: '/login',
    name: 'user-login',
    component: UserLogin
  },
  {
    path: '/registration',
    name: 'user-registration',
    component: UserRegistration
  },
  {
    path: '/confirmSignup',
    name: 'confirm-signup',
    component: UserConfirmSignup
  },
  {
    path: '/forgotPassword',
    name: 'forgot-password',
    component: UserForgotPassword
  },
  {
    path: '/passwordRecovery',
    name: 'password-recovery',
    component: UserPasswordRecovery
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
